package fr.jihadoussad.heimdall.server.contract;

import java.util.ArrayList;
import java.util.List;

public class RegistrationAppOutput {

    private String publicKey;

    private final List<String> privileges;

    public RegistrationAppOutput() {
        this.privileges = new ArrayList<>();
    }

    public RegistrationAppOutput(final String publicKey, final List<String> privileges) {
        this.publicKey = publicKey;
        this.privileges = privileges;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public List<String> getPrivileges() {
        return privileges;
    }
}
