package fr.jihadoussad.heimdall.server.contract;

import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * AuthenticationAdminService
 */
public interface AuthenticationApplicationService {

    /**
     * This method permit to ban a user for an application. If user not found, fallback...
     * @param login user to ban.
     * @param appName the application name.
     */

    void banUser(String login, String appName);

    /**
     * This method permit to register application.
     * @param appName to register.
     * @param password the password.
     * @param defaultPrivilege the default privilege.
     * @param secretKey the secret key generated.
     * @param tokenExpiration the expiration time for user authentication token.
     * @return the public key to cipher contents and heimdall privileges access.
     * @throws ContractValidationException if application name should not be filled correctly or exist.
     */
    RegistrationAppOutput registerApplication(final String appName, final String password,
                                              final String defaultPrivilege, final String secretKey, final Long tokenExpiration)
            throws ContractValidationException, NoSuchAlgorithmException, IOException;

    /**
     * This method permit to unsubscribe application. If application not found, rollback...
     * @param appName to unsubscribe.
     */
    void unsubscribeApplication(final String appName);

    /**
     * This method permit to add privileges for a targeted application.
     * @param appName target.
     * @param privileges to add.
     * @throws ContractValidationException if targeted application or privilege name should not be filled correctly.
     * It exception can also be throws if no application found.
     */
    void addPrivileges(final String appName, final String... privileges) throws ContractValidationException;

    /**
     * This method permit to remove privileges for a targeted application. If application or privilege not found, fallback...
     * @param appName target.
     * @param privileges to remove.
     * @throws ContractValidationException If default privilege application should be filled.
     */
    void removePrivileges(final String appName, final String... privileges) throws ContractValidationException;

    /**
     * This method permit to get the expiration of authentication user token.
     * @param appName the application name.
     * @return the expiration token time.
     * @throws ContractValidationException If the application name filled does not exist.
     */
    Long getExpirationToken(final String appName) throws ContractValidationException;
}
