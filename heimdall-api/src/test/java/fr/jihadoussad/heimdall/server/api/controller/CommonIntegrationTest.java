package fr.jihadoussad.heimdall.server.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.heimdall.server.api.HeimdallApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.concurrent.atomic.AtomicReference;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.options;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = HeimdallApplication.class)
@ComponentScan(basePackages = "fr.jihadoussad.heimdall.server.*")
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CommonIntegrationTest {

    protected static final String URL_OAUTH2_SERVER = "/oauth2/heimdall";
    protected static final String URL_REGISTER_SERVER = "/register/app";

    protected static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Autowired
    protected MockMvc mvc;

    protected String appToken;

    public void getToken() throws Exception {
        final AtomicReference<String> token = new AtomicReference<>("");

        mvc.perform(options(URL_OAUTH2_SERVER + "/accessToken?login=appTest&password=AppT3st!")
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com"));

        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken?login=appTest&password=AppT3st!")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        appToken = token.get();
    }
}
