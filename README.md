# Heimdall

**State build**: [![pipeline status](https://gitlab.com/oussad.jihad/heimdal/badges/master/pipeline.svg)](https://gitlab.com/oussad.jihad/heimdal/-/commits/master)

**Jacoco report**: [![jacoco report](https://gitlab.com/oussad.jihad/heimdal/badges/master/coverage.svg)](https://gitlab.com/oussad.jihad/heimdal/-/commits/master)

## Test and Dev datas

| Application | Default privilege |                                                                                                                                                                                                public key                                                                                                                                                                                                |
|:-----------:|:-----------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|   appTest   |        USER       | MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmHRq03+VVIlpr06i5SEbbpq3jmjie98/NrNPJ+pZpOAG9IFoR35wi3eAVIbZhHwTiubTXl9ck3rHgsBzjZ5em3uYJtaSyJv/K2k4HhVCbyEaodVAxjditloFrOYq9WCGbiRPbQsqzqG2YXHHiEqMM+sCEhRoqs3yyk99CH1N/9hriqTzEvi063xSGNQeaB6F9yx1jdFq5ZZwXqpe6ry7PsywkvAjACphF+O3FTOhPaToQeaogRXftNgg652H8pH0yTwsUNXagWPfQFNxDxPjWrTCA8siOdNiDu6YeW2wBBixzoN1sLHbps2ppIEGbkryWFuuw6aAd89qys0phDNr8QIDAQAB |
|   appMock   |        USER       | MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm/Q7/uTlFtRoz/DjSu1xkllOlvUnZqmX4HFdLbR9OOHC9ts8XMlqvphRPvr+hl9yjHMLX1t+krKvmBYzFxmVuU9KEwJ4yoq3Qo5AP117zEn+m8TKSBoipyEsoGFaQQKJ/JImdRmwUoOMEJnJbrx2fEyRugxPvJkFmhmnXgJ96YIFl/VRqBZRtWEPaocg6O3AVvzi8n+3ES6sTiMq+zyukm/oEsF/kHdvfj98+luYhPGMBArKihXcInH1KHnBz56HqdsVeD1bDeAEUJs3h+nMxvbfPM6qMjoxMhLEuzsWk21WYZSnmGho6g92nvsEg5YoGvrPg0U43x2YRbm/+KJmGQIDAQAB |
|   heimdall  |        APP        | MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAghjwzl8tinRcZgVVJLI3zyzVL1pGjCOKmlxjp1BtCVOWbOc/3VO0Lu65N0sH8oQXZVw8DkNMnDfoEWne7QEM5zOJau58IbO7VM6Y5TkYgY03TlbeB2VFtqo4MxsaWoo5JECFmNvuJSLydAQEgm+J+gZoHK3BbRzM38z0Rjqqoo5pq84WbcFK226POxnxm34NNSUXO7Kuw53ZxZ192ktnQW9CeCEINn1oMxJJRUvh/mKDMx+mZMrO4JI1ry35NtZTQ+k6os4b690XI0RfKe5M/H3mad1vI3VKF5rzy966uHK57cHIHtkWlYxmVp9fDr8klWSoSibXSOor12smJxSXYQIDAQAB |


## Frameworks and API used

- https://spring.io/projects/spring-boot
- https://www.eclemma.org/jacoco/
- https://assertj.github.io/doc/
- https://github.com/jwtk/jjwt/
- https://github.com/ulisesbocchio/jasypt-spring-boot
- https://github.com/DependencyTrack/dependency-track

### Utils for documentation

- https://www.tablesgenerator.com/markdown_tables