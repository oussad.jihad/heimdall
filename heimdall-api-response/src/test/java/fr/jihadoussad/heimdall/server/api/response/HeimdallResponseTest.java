package fr.jihadoussad.heimdall.server.api.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class HeimdallResponseTest {

    private static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Test
    public void writeResponseJsonValueTest() throws JsonProcessingException {
        final HeimdallResponse expected = new HeimdallResponse("00", "OK!");

        final HeimdallResponse actual = OBJECT_MAPPER.readValue(expected.toString(), HeimdallResponse.class);

        assertThat(actual.getCode()).isEqualTo(expected.getCode());
        assertThat(actual.getMessage()).isEqualTo(expected.getMessage());
    }
}
