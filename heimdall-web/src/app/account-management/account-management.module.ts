import { NgModule } from '@angular/core';

import {AccountManagementComponent} from "./account-management.component";
import {AuthGuard} from "../auth-guard.service";

@NgModule({
  declarations: [
    AccountManagementComponent,
  ],
  imports: [],
  providers: [AuthGuard],
  bootstrap: [AccountManagementComponent]
})
export class AccountManagementModule { }
