import { Injectable } from '@angular/core';
// RxJS 6
import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable()
export class AuthService {
  isLoggedIn: boolean = false; // L'utilisateur est-il connecté ?
  redirectUrl: string; // où rediriger l'utilisateur après l'authentification ?

  private heimdallAuthenticationUrl = 'http://127.0.0.1:8000/oauth2/heimdall/accessToken';

  constructor(private http: HttpClient) {}

  private static log(message: string) {
    console.info(message);
  }

  private handleError<T>(operation='operation', result?: T) {
    return (error: any): Observable<T> => {
      console.info(error);
      console.info(`${operation} failed: ${error.message}`);

      return of(result as T);
    }
  }

  // Une méthode de connexion
  login(name: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      params: new HttpParams().append('login', name).append('password', password)
    };

    console.info(httpOptions);

    this.http.get<string>(this.heimdallAuthenticationUrl, httpOptions)
      .subscribe(
        data => console.log('success', data),
        error => console.log('oops', error)
      );

    console.info("appel terminé");
  }

  // Une méthode de déconnexion
  logout(): void {
    this.isLoggedIn = false;
  }
}
