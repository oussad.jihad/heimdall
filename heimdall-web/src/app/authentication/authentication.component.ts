import { Component, OnInit } from '@angular/core';
import { Credential } from "./model/credential";
import {AuthService} from "./service/auth.service";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  credential: Credential;

  isLoginValid: boolean;
  isPasswordValid: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.credential = new Credential("", "");
    this.isLoginValid = true;
    this.isPasswordValid = true;
  }

  onSubmit() {
    this.authService.login(this.credential.login, this.credential.password)
  }
}
