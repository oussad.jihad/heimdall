package fr.jihadoussad.heimdall.server.core.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    private String login;

    private String password;

    private String secretKey;

    @ManyToOne
    private Application application;

    @ManyToMany(fetch = FetchType.EAGER)
    private final List<Privilege> privileges;

    public User() {
        this.privileges = new ArrayList<>();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void addPrivileges(final Privilege... privileges) {
        this.privileges.addAll(Arrays.asList(privileges));
        Arrays.stream(privileges)
                .forEach(privilege -> privilege.getUsers().add(this));
    }

    public void removePrivileges(final Privilege... privileges) {
        this.privileges.removeAll(Arrays.asList(privileges));
        Arrays.stream(privileges)
                .forEach(privilege -> privilege.getUsers().remove(this));
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
