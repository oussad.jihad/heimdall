package fr.jihadoussad.heimdall.server.core.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Privilege {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long privilegeId;

    private String name;

    @ManyToOne
    private Application application;

    @ManyToMany(mappedBy = "privileges", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private final List<User> users;

    public Privilege() {
        users = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(final Application application) {
        this.application = application;
    }

    public List<User> getUsers() {
        return users;
    }

    public void removeUsers() {
        users.forEach(user -> user.getPrivileges().remove(this));
    }
}
