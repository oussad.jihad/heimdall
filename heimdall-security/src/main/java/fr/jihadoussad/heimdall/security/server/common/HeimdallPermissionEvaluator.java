package fr.jihadoussad.heimdall.security.server.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public class HeimdallPermissionEvaluator implements PermissionEvaluator {

    private final Logger logger = LoggerFactory.getLogger(HeimdallPermissionEvaluator.class);

    @Override
    public boolean hasPermission(final Authentication authentication, final Object appName, final Object permission) {
        logger.info("[SECURITY][PERMISSION] BEGIN - Processing {} permission...", appName);
        if ((authentication == null) || !(appName instanceof String) || !(permission instanceof String)){
            return false;
        }

        final boolean hasPermission = hasPrivilege(authentication, appName.toString(), permission.toString().toUpperCase());
        logger.info("[SECURITY][PERMISSION] END - Permission {} for user {} to access {} resource", hasPermission?"allowed":"denied", authentication.getName(), appName);
        return hasPermission;
    }

    @Override
    public boolean hasPermission(final Authentication authentication, final Serializable targetId, final String appName, final Object permission) {
        logger.info("[SECURITY][PERMISSION] Processing {} permission...", appName);
        if ((authentication == null) || (appName == null) || !(permission instanceof String)) {
            return false;
        }

        final boolean hasPermission = hasPrivilege(authentication, appName, permission.toString().toUpperCase());
        logger.info("[SECURITY][PERMISSION] END - Permission {} for user {} to access {} resource", hasPermission?"allowed":"denied", authentication.getName(), appName);
        return hasPermission;
    }

    private boolean hasPrivilege(final Authentication authentication, final String appName, final String permission) {
        return authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(authority -> authority.equals(permission)) && (authentication.getName().equals(appName) || "heimdall".equals(appName));
    }
}
