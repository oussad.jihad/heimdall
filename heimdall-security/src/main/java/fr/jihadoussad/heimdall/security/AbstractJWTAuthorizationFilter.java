package fr.jihadoussad.heimdall.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractJWTAuthorizationFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(AbstractJWTAuthorizationFilter.class);

    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

    public AbstractJWTAuthorizationFilter() {}

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain chain) throws IOException, ServletException {
        logger.info("[SECURITY][AUTHORIZATION] BEGIN - Attempt to access {} resource with {} http method from {} address", request.getPathInfo(), request.getMethod(), request.getRemoteAddr());
        final String header = request.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            logger.info("[SECURITY][AUTHORIZATION] END - Token not found. Attempt to identify the authority on this resource without token...");
            chain.doFilter(request, response);
            return;
        }

        logger.info("[SECURITY][AUTHORIZATION] Token found. Process checking validity...");
        final UsernamePasswordAuthenticationToken authentication = getAuthentication(header.replace(TOKEN_PREFIX, ""));

        logger.info("[SECURITY][AUTHORIZATION] END - Checking validity token finish. Attempt to identify the authority on this resource...");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    public abstract ResponseEntity<Map<String, Object>> checkToken(final String token);

    private UsernamePasswordAuthenticationToken getAuthentication(final String token) {
        final ResponseEntity<Map<String, Object>> response = checkToken(token);

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            @SuppressWarnings("unchecked")
            final List<String> authorities = (List<String>) Objects.requireNonNull(response.getBody()).get("privileges");
            final String appName = response.getBody().get("appName").toString();
            logger.info("[SECURITY][AUTHORIZATION] Token valid. Welcome {}", appName);
            return new UsernamePasswordAuthenticationToken(Objects.requireNonNull(response.getBody()).get("sub"), null, getAuthorities(authorities, appName));
        }
        logger.info("[SECURITY][AUTHORIZATION] Token invalid. Checking validity finish with following http status: {}.", response.getStatusCode());
        return null;
    }

    private List<SimpleGrantedAuthority> getAuthorities(final List<String> privileges, final String appName) {
        return privileges
                .stream()
                .map(privilege -> "ROLE_" + privilege + "_" + appName.toUpperCase())
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
