package fr.jihadoussad.heimdall.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

public abstract class AbstractWebSecurity extends AbstractHttpSecurity {

    @Value("${heimdall.allowed.origin}")
    private String allowedOrigin;

    protected abstract AbstractJWTAuthorizationFilter jwtAuthorizationFilter();

    @Bean
    CorsFilter corsFilter() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin(allowedOrigin);
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        super.configure(http);

        // Activate Cross Origin Resource Sharing
        http.cors();

        // Activate Cross Site Request Forgery
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
}
